namespace Logger;

/// <summary>
/// Allows the user to list all entries in the log.
/// </summary>
public class List : BaseOperation
{
    public List() : base(new HashSet<string?>(){"L", "l"}) {}

    public override void Print(TextWriter writer)
    {
        writer.Write("[L]ist");
    }

    public override ReturnValue Run(List<LogEntry> logEntries, TextReader reader, TextWriter writer)
    {
        for (var i = 0; i < logEntries.Count; i++)
        {
            writer.WriteLine($"{i + 1}: {logEntries[i].ToString()}");
        }

        return ReturnValue.Ok;
    }
}