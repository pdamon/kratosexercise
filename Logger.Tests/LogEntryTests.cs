namespace Logger.Tests;

[TestFixture]
public class LogEntryTests
{
    private LogEntry _logEntry = null!;
    private DateTime _startTime;
    
    [SetUp]
    public void Setup()
    {
        _startTime = DateTime.Now;
        _logEntry = new LogEntry("This is some data");
    }
    
    [Test]
    public void LogEntry_Ctor()
    {
        Assert.That(_logEntry.TimeStamp, Is.GreaterThan(_startTime));
        Assert.That(_logEntry.TimeStamp, Is.LessThan(DateTime.Now));
        Assert.That(_logEntry.Data, Is.EqualTo("This is some data"));
    }

    [Test]
    public void LogEntry_ToString()
    {
        var output = _logEntry.ToString();
        
        Assert.That(output[..8], Is.EqualTo("\"time\": "));
        Assert.That(output[^27..], Is.EqualTo("\"data\": \"This is some data\""));
    }
}