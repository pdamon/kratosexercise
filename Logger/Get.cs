using System.Text.RegularExpressions;

namespace Logger;

/// <summary>
/// Allows the user to get a list of matching entries from the log.
/// </summary>
public class Get : BaseOperation
{
    public Get() : base(new HashSet<string?>(){"G", "g"}) {}

    public override void Print(TextWriter writer)
    {
        writer.Write("[G]et");
    }

    public override ReturnValue Run(List<LogEntry> logEntries, TextReader reader, TextWriter writer)
    {
        writer.WriteLine("Enter the value you would like to search for:");
        var input = reader.ReadLine();

        if (input == null)
        {
            writer.WriteLine("Invalid input.");
            throw new LoggerException();
        }

        var regex = new Regex(input);

        writer.WriteLine("Logged Data:");
        writer.WriteLine("------------------------");

        for(var i = 0; i < logEntries.Count; i++)
        {
            if (logEntries[i].ToString().Contains(input))
            {
                writer.WriteLine($"{i + 1}: {logEntries[i]}");
            }
        }
        writer.WriteLine("------------------------");

        return ReturnValue.Ok;
    }
}