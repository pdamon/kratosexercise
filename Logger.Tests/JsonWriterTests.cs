namespace Logger.Tests;

[TestFixture]
public class JsonWriterTests
{
    private StringWriter _writer = null!;
    private List<LogEntry> _entries = null!;
    private JsonWriter _jsonWriter = null!;
    
    [SetUp]
    public void Setup()
    {
        _writer = new StringWriter();
        _jsonWriter = new JsonWriter(_writer);
        
        _entries = new List<LogEntry>();
    }

    [Test]
    public void JsonWriter_Setup()
    {
        var testWriter = new StringWriter();
        
        
    }

    [Test]
    public void JsonWriter_SaveToFile_Empty()
    {
        _jsonWriter.SaveToFile(_entries);
        
        var output = _writer.ToString().Split("\n");
        
        Assert.That(output, Has.Length.EqualTo(3));

        Assert.That(output.ElementAt(0), Is.EqualTo("["));
        Assert.That(output.ElementAt(1), Is.EqualTo("]"));
        Assert.That(output.ElementAt(2), Is.EqualTo(string.Empty));
    }
    
    [Test]
    public void JsonWriter_SaveToFile_Simple()
    {
        _entries.Add(new LogEntry("First Log Entry"));
        
        _jsonWriter.SaveToFile(_entries);
        
        var output = _writer.ToString().Split("\n");
        
        Assert.That(output, Has.Length.EqualTo(4));
        Assert.That(output.ElementAt(0), Is.EqualTo("["));
        Assert.That(output.ElementAt(1)[..10], Is.EqualTo("{\"time\": \""));
        Assert.That(output.ElementAt(1)[^26..], Is.EqualTo("\"data\": \"First Log Entry\"}"));
        Assert.That(output.ElementAt(2), Is.EqualTo("]"));
        Assert.That(output.ElementAt(3), Is.EqualTo(string.Empty));
    }
    
    [Test]
    public void JsonWriter_SaveToFile_Complex()
    {
        _entries.Add(new LogEntry("First Log Entry"));
        _entries.Add(new LogEntry("2nd Log Entry"));
        _entries.Add(new LogEntry("Third Log Entry"));
        
        
        _jsonWriter.SaveToFile(_entries);
        
        var output = _writer.ToString().Split("\n");
        
        Assert.That(output, Has.Length.EqualTo(6));
        Assert.That(output.ElementAt(0), Is.EqualTo("["));
        Assert.That(output.ElementAt(1)[..10], Is.EqualTo("{\"time\": \""));
        Assert.That(output.ElementAt(1)[^27..], Is.EqualTo("\"data\": \"First Log Entry\"},"));
        Assert.That(output.ElementAt(2)[..10], Is.EqualTo("{\"time\": \""));
        Assert.That(output.ElementAt(2)[^25..], Is.EqualTo("\"data\": \"2nd Log Entry\"},"));
        Assert.That(output.ElementAt(3)[..10], Is.EqualTo("{\"time\": \""));
        Assert.That(output.ElementAt(3)[^26..], Is.EqualTo("\"data\": \"Third Log Entry\"}"));
        Assert.That(output.ElementAt(4), Is.EqualTo("]"));
        Assert.That(output.ElementAt(5), Is.EqualTo(string.Empty));
    }
}