namespace Logger;

/// <summary>
/// An interface for operations in the Console based logger.
/// </summary>
public interface IOperation
{
    /// <summary>
    /// Prints out the name of the operation, so it can be selected.
    /// </summary>
    /// <param name="writer"> The TextWriter for the output to be written to</param>
    void Print(TextWriter writer);

    /// <summary>
    /// Checks if the given text matches the keybindings for this operation.
    /// </summary>
    /// <param name="text">The text to match</param>
    /// <returns>True if a match, false otherwise</returns>
    bool CheckKeyBinds(string? text);
    
    /// <summary>
    /// Runs the operation.
    /// </summary>
    /// <param name="logEntries">A list of log entries that can be processed or modified</param>
    /// <param name="reader">A TextReader that text can be read from</param>
    /// <param name="writer">A TextWriter that text can be written to</param>
    /// <returns>A ReturnValue, it should always be Ok, unless you want the program to terminate</returns>
    ReturnValue Run(List<LogEntry> logEntries, TextReader reader, TextWriter writer);
}