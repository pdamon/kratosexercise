namespace Logger;

/// <summary>
/// An enum type for tracking the return value of operations.
/// </summary>
public enum ReturnValue
{
    Ok,
    Exit
}