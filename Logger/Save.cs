namespace Logger;

/// <summary>
/// Allows the user to save the log to a file.
/// </summary>
public class Save : BaseOperation
{
    private readonly IWriter _writer = null!;
    public Save() : base(new HashSet<string?>() { "S", "s" })
    {}

    /// <summary>
    /// A Constructor that's nice for testing
    /// </summary>
    /// <param name="writer">The writer to use</param>
    public Save(IWriter writer) : base(new HashSet<string?>() { "S", "s" })
    {
        _writer = writer;
    }

    public override void Print(TextWriter writer)
    {
        writer.Write("[S]ave");
    }

    public override ReturnValue Run(List<LogEntry> logEntries, TextReader reader, TextWriter writer)
    {
        _writer.Setup(reader, writer);
        _writer.SaveToFile(logEntries);
        
        writer.WriteLine("File Saved.");

        return ReturnValue.Ok;
    }
}