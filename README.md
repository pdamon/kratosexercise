# kratos exercise

Exercise requirements:
- Create a logging storage program using C#
- Console based program that presents text in a console and allows the user to make selections (operations)
- Supported operations will be
	- Add a log entry (a log is a text string), each log has a timestamp associated with it.
	- List all log entries.
	- Get log(s) based on a string search criterea entered by the user.
	- Save log to file in a json format.

Note: We are looking for quality of work as opposed to how fast it can be completed.
