﻿using System.Runtime.CompilerServices;
using Logger;

class Program
{
    private readonly List<BaseOperation> _operations;
    private readonly List<LogEntry> _logEntries;
    private readonly TextWriter _writer;
    private readonly TextReader _reader;
    
    /// <summary>
    /// Construct a new Program with lots of dependency injection.
    /// </summary>
    /// <param name="operations">A list of IOperations the program can perform</param>
    /// <param name="writer">A TextWriter to write output to.</param>
    /// <param name="reader">A TextReader to read input from.</param>
    public Program(List<BaseOperation> operations,TextWriter writer, TextReader reader)
    {
        _operations = operations;
        _logEntries = new List<LogEntry>();
        _writer = writer;
        _reader = reader;
    }

    /// <summary>
    /// Prints the menu for the user to select an option.
    /// </summary>
    private void PrintMenu()
    {
        _writer.WriteLine("What do you want to do?");

        foreach (var op in _operations)
        {
            op.Print(_writer);
            _writer.Write(" ");
        }
        _writer.WriteLine();
    }

    /// <summary>
    /// Runs the users input.
    /// </summary>
    /// <returns>Either Ok if the program should continue or Exit if the program should exit</returns>
    /// <exception cref="LoggerException">An exception is thrown in invalid input is found.</exception>
    private ReturnValue RunInput()
    {
        var input = _reader.ReadLine();
        var selectedOps = _operations.Where(op => op.CheckKeyBinds(input)).ToArray();
            
        if (selectedOps.Length != 1)
        {
            throw new LoggerException();
        } else
        {
            return selectedOps.ElementAt(0).Run(_logEntries, _reader, _writer);
        }
    }
    
    static void Main(string[] args)
    {
        // initialize things and dependency inject all the things
        var operations = new List<BaseOperation>();
        var reader = Console.In;
        var writer = Console.Out;
        var jsonWriter = new JsonWriter();
        
        operations.Add(new Add());
        operations.Add(new List());
        operations.Add(new Get());
        operations.Add(new Save(jsonWriter));
        operations.Add(new Quit());
        
        var theProgram = new Program(operations, writer, reader);
        
        // main program loop
        while (true)
        {
            theProgram.PrintMenu();

            try
            {
                var rv = theProgram.RunInput();
                switch (rv)
                {
                    case ReturnValue.Ok:
                        break;
                    case ReturnValue.Exit:
                        return;
                    default:
                        theProgram._writer.WriteLine("How did you get here. This case shouldn't exits.");
                        break;
                }
            }
            catch (LoggerException)
            {
                theProgram._writer.WriteLine("An Exception was thrown. Please try again.");
            }
        }
    }
}
