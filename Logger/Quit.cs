namespace Logger;

/// <summary>
/// Allows the user to quit the program.
/// </summary>
public class Quit : BaseOperation
{
    public Quit() : base(new HashSet<string?>() { "Q", "q" }) {}

    public override void Print(TextWriter writer)
    {
        writer.Write("[Q]uit");
    }

    public override ReturnValue Run(List<LogEntry> logEntries, TextReader reader, TextWriter writer)
    {
        writer.WriteLine("Quitting...");

        return ReturnValue.Exit;
    }
}