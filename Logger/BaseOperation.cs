namespace Logger;

/// <summary>
/// Base class that all operations must inherit from.
/// </summary>
public abstract class BaseOperation : IOperation
{
    private readonly HashSet<string?> _keyBinds; // the key bindings that will trigger a given operation

    /// <summary>
    /// Constructs an operation given a set of keybindings
    /// </summary>
    /// <param name="keyBinds">The set of keybindings</param>
    protected BaseOperation(HashSet<string?> keyBinds)
    {
        _keyBinds = keyBinds;
    }
    
    public bool CheckKeyBinds(string? text)
    {
        return _keyBinds.Contains(text);
    }
    
    public abstract void Print(TextWriter writer);

    public abstract ReturnValue Run(List<LogEntry> logEntries, TextReader reader, TextWriter writer);
}