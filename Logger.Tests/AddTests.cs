namespace Logger.Tests;

[TestFixture]
public class AddTests
{
    private List<LogEntry> _entries = null!;
    private StringWriter _writer = null!;
    private StringReader _reader = null!;
    private Add _add = null!;
    
    [SetUp]
    public void Setup()
    {
        _writer = new StringWriter();
        _reader = new StringReader("");
        _add = new Add();
        
        _entries = new List<LogEntry>();
    }

    [Test]
    public void Add_KeyBinds()
    {
        Assert.That(_add.CheckKeyBinds("A"), Is.True);
        Assert.That(_add.CheckKeyBinds("a"), Is.True);
        Assert.That(_add.CheckKeyBinds("q"), Is.False);
        Assert.That(_add.CheckKeyBinds("S"), Is.False);
        Assert.That(_add.CheckKeyBinds("Something Random"), Is.False);
    }

    [Test]
    public void Add_Print()
    {
        _add.Print(_writer);
        Assert.That(_writer.ToString(), Is.EqualTo("[A]dd"));
    }

    [Test]
    public void Add_Simple()
    {
        Assert.That(_entries, Has.Count.EqualTo(0));
        
        _reader = new StringReader("1st test thing\n");
        var rv = _add.Run(_entries, _reader, _writer);

        Assert.That(rv, Is.EqualTo(ReturnValue.Ok));
        Assert.That(_entries, Has.Count.EqualTo(1));
        Assert.That(_entries.ElementAt(0).TimeStamp, Is.LessThan(DateTime.Now));
        Assert.That(_entries.ElementAt(0).Data, Is.EqualTo("1st test thing"));
        Assert.That(_writer.ToString(), Is.EqualTo("Enter the data you want to log. Then press ENTER:\nAdded New Data to the Log.\n"));
    }

    [Test]
    public void Add_Complex()
    {
        Assert.That(_entries, Has.Count.EqualTo(0));
        
        _reader = new StringReader("1st test thing\n");
        var rv1 = _add.Run(_entries, _reader, _writer);
        _reader = new StringReader("2nd test thing\n");
        var rv2 = _add.Run(_entries, _reader, _writer);
        _reader = new StringReader("3rd test thing\n");
        var rv3 = _add.Run(_entries, _reader, _writer);
        
        Assert.That(rv1, Is.EqualTo(ReturnValue.Ok));
        Assert.That(rv2, Is.EqualTo(ReturnValue.Ok));
        Assert.That(rv3, Is.EqualTo(ReturnValue.Ok));
        
        Assert.That(_entries, Has.Count.EqualTo(3));
        
        Assert.That(_entries.ElementAt(0).TimeStamp, Is.LessThan(_entries.ElementAt(1).TimeStamp));
        Assert.That(_entries.ElementAt(0).Data, Is.EqualTo("1st test thing"));
        
        Assert.That(_entries.ElementAt(1).TimeStamp, Is.LessThan(_entries.ElementAt(2).TimeStamp));
        Assert.That(_entries.ElementAt(1).Data, Is.EqualTo("2nd test thing"));

        Assert.That(_entries.ElementAt(2).TimeStamp, Is.LessThan(DateTime.Now));
        Assert.That(_entries.ElementAt(2).Data, Is.EqualTo("3rd test thing"));
            
        Assert.That(_writer.ToString(), Is.EqualTo("Enter the data you want to log. Then press ENTER:\nAdded New Data to the Log.\nEnter the data you want to log. Then press ENTER:\nAdded New Data to the Log.\nEnter the data you want to log. Then press ENTER:\nAdded New Data to the Log.\n"));
    }
}