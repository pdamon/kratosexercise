namespace Logger;

/// <summary>
/// Allows for the user to add a new entry to the log.
/// </summary>
public class Add : BaseOperation {
    public Add() : base(new HashSet<string?>() { "A", "a" }) {}

    public override void Print(TextWriter writer)
    {
        writer.Write("[A]dd");
    }

    public override ReturnValue Run(List<LogEntry> logEntries, TextReader reader, TextWriter writer)
    {
        writer.WriteLine("Enter the data you want to log. Then press ENTER:");
        
        var data = reader.ReadLine();
        var newEntry = new LogEntry(data);
        
        logEntries.Add(newEntry);
        
        writer.WriteLine("Added New Data to the Log.");
        
        return ReturnValue.Ok;
    }
}