namespace Logger.Tests;

[TestFixture]
public class QuitTests
{
    private List<LogEntry> _entries = null!;
    private StringWriter _writer = null!;
    private StringReader _reader = null!;
    private Quit _quit = null!;
    
    [SetUp]
    public void Setup()
    {
        _writer = new StringWriter();
        _reader = new StringReader("");
        _entries = new List<LogEntry>();
        _quit = new Quit();
    }
    
    [Test]
    public void Quit_KeyBinds()
    {
        Assert.That(_quit.CheckKeyBinds("Q"), Is.True);
        Assert.That(_quit.CheckKeyBinds("q"), Is.True);
        Assert.That(_quit.CheckKeyBinds("a"), Is.False);
        Assert.That(_quit.CheckKeyBinds("W"), Is.False);
        Assert.That(_quit.CheckKeyBinds("Something Random"), Is.False);
    }

    [Test]
    public void Quit_Print()
    {
        _quit.Print(_writer);
        Assert.That(_writer.ToString(), Is.EqualTo("[Q]uit"));
    }

    [Test]
    public void Quit_Quit()
    {
        var rv1 = _quit.Run(_entries, _reader, _writer);
        
        Assert.That(rv1, Is.EqualTo(ReturnValue.Exit));
    }
}