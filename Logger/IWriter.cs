namespace Logger;

/// <summary>
/// An interface for writing the log entries to a file/database/something else.
/// </summary>
public interface IWriter
{
    /// <summary>
    /// Sets up the writer with FileIO or Database etc.
    /// </summary>
    /// <param name="reader">A TextReader for user input</param>
    /// <param name="writer">A TextWriter for user output</param>
    void Setup(TextReader reader, TextWriter writer);
    
    /// <summary>
    /// Saves the given list of entries to a file/database
    /// </summary>
    /// <param name="data">The entries to save</param>
    void SaveToFile(List<LogEntry> data);
}