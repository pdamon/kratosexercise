namespace Logger;

/// <summary>
/// A logger exception. It doesn't really do much.
/// </summary>
public class LoggerException : Exception
{
    public LoggerException()
    {
    }
}