namespace Logger;

/// <summary>
/// A class which writes log entries to a JSON file
/// </summary>
public class JsonWriter : IWriter
{
    private readonly TextWriter? _writer; // a writer to write the JSON to
    private string? _outputPath;
    /// <summary>
    /// Constructs a new JsonWriter
    /// </summary>
    public JsonWriter() {}

    /// <summary>
    /// A constructor that's useful for testing and DI
    /// </summary>
    /// <param name="writer">A writer to be written to</param>
    public JsonWriter(TextWriter writer)
    {
        _writer = writer;
    }

    /// <summary>
    /// Sets up the JSON Writer
    /// </summary>
    /// <param name="reader">A TextReader for user input</param>
    /// <param name="writer">A TextWriter for user output</param>
    /// <exception cref="LoggerException">Throws if the input filepath is null</exception>
    public void Setup(TextReader reader, TextWriter writer)
    {
        writer.WriteLine("Enter the path of the file you would like to write to:");
        var input = reader.ReadLine();

        _outputPath = input ?? throw new LoggerException();
    }

    /// <summary>
    /// Saves the give data to JSON
    /// </summary>
    /// <param name="data">A list of log entries to save</param>
    /// <exception cref="LoggerException">Occurs when no writer is available</exception>
    public void SaveToFile(List<LogEntry> data)
    {
        if (_outputPath == null)
        {
            if (_writer == null)
            {
                throw new LoggerException();
            }
            write(data, _writer);
            return;
        }
        
        using var writer = new StreamWriter(this._outputPath);
        write(data, writer);
    }

    private void write(List<LogEntry> data, TextWriter writer)
    {
        writer.WriteLine("[");
        for (var i = 0; i < data.Count; i++)
        {
            var entry = data[i];

            writer.Write("{");
            writer.Write($"\"time\": \"{entry.TimeStamp}\",");
            writer.Write($"\"data\": \"{entry.Data}\"");
            writer.Write("}");

            if (i < data.Count - 1)
            {
                writer.WriteLine(",");
            }
            else
            {
                writer.WriteLine();
            }
        }

        writer.WriteLine("]");
    }
}