namespace Logger.Tests;

[TestFixture]
public class ListTests
{
    private List<LogEntry> _entries = null!;
    private StringWriter _writer = null!;
    private StringReader _reader = null!;
    private List _list = null!;
    
    [SetUp]
    public void Setup()
    {
        _writer = new StringWriter();
        _reader = new StringReader("");
        _list = new List();
        
        _entries = new List<LogEntry>()
        {
            new LogEntry("1st Log Entry")
        };
    }

    [Test]
    public void List_KeyBinds()
    {
        Assert.That(_list.CheckKeyBinds("L"), Is.True);
        Assert.That(_list.CheckKeyBinds("l"), Is.True);
        Assert.That(_list.CheckKeyBinds("q"), Is.False);
        Assert.That(_list.CheckKeyBinds("O"), Is.False);
        Assert.That(_list.CheckKeyBinds("Something Random"), Is.False);
    }

    [Test]
    public void List_Print()
    {
        _list.Print(_writer);
        Assert.That(_writer.ToString(), Is.EqualTo("[L]ist"));
    }

    [Test]
    public void List_Simple()
    {
        var rv1 = _list.Run(_entries, _reader, _writer);
        
        var output = _writer.ToString();
        
        Assert.That(rv1, Is.EqualTo(ReturnValue.Ok));
        Assert.That(output[..11], Is.EqualTo("1: \"time\": "));
        Assert.That(output[^24..], Is.EqualTo("\"data\": \"1st Log Entry\"\n"));
    }

    [Test]
    public void List_Complex()
    {
        _entries.Add(new LogEntry("2nd Log Entry"));
        _entries.Add(new LogEntry("3rd Log Entry"));
        
        var rv1 = _list.Run(_entries, _reader, _writer);
        
        var output = _writer.ToString().Split("\n");
        Assert.That(rv1, Is.EqualTo(ReturnValue.Ok));
        Assert.That(output[0][..11], Is.EqualTo("1: \"time\": "));
        Assert.That(output[0][^23..], Is.EqualTo("\"data\": \"1st Log Entry\""));
        Assert.That(output[1][..11], Is.EqualTo("2: \"time\": "));
        Assert.That(output[1][^23..], Is.EqualTo("\"data\": \"2nd Log Entry\""));
        Assert.That(output[2][..11], Is.EqualTo("3: \"time\": "));
        Assert.That(output[2][^23..], Is.EqualTo("\"data\": \"3rd Log Entry\""));
    }
}