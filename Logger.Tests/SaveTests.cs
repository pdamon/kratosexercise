namespace Logger.Tests;

class MockFileWriter : IWriter
{
    private readonly TextWriter _writer;
    
    public MockFileWriter(TextWriter writer)
    {
        this._writer = writer;
    }

    public void Setup(TextReader reader, TextWriter writer)
    {
        writer.WriteLine("Setup was called.");
    }

    public void SaveToFile(List<LogEntry> data)
    {
        _writer.WriteLine("SaveToFile was called.");
    }
}

[TestFixture]
public class SaveTests
{
    private StringWriter _writer = null!;
    private StringReader _reader = null!;
    private IWriter _iwriter = null!;
    private Save _save = null!;
    
    [SetUp]
    public void Setup()
    {
        _writer = new StringWriter();
        _iwriter = new MockFileWriter(_writer);
        _save = new Save(_iwriter);
    }

    [Test]
    public void Save_KeyBinds()
    {
        Assert.That(_save.CheckKeyBinds("S"), Is.True);
        Assert.That(_save.CheckKeyBinds("s"), Is.True);
        Assert.That(_save.CheckKeyBinds("a"), Is.False);
        Assert.That(_save.CheckKeyBinds("W"), Is.False);
        Assert.That(_save.CheckKeyBinds("Something Random"), Is.False);
    }

    [Test]
    public void Save_Print()
    {
        _save.Print(_writer);
        Assert.That(_writer.ToString(), Is.EqualTo("[S]ave"));
    }

    [Test]
    public void Save_Run()
    {
        var rv1 = _save.Run(new List<LogEntry>(), _reader, _writer);
        
        Assert.That(rv1, Is.EqualTo(ReturnValue.Ok));
        Assert.That(_writer.ToString(), Is.EqualTo("Setup was called.\nSaveToFile was called.\nFile Saved.\n"));
        
    }
}