namespace Logger;

/// <summary>
/// Stores a single log entry
/// </summary>
public class LogEntry
{
    /// <summary>
    /// Constructs a new log entry. The time of the entry is determined by DateTime.Now
    /// </summary>
    /// <param name="data">The data to log</param>
    public LogEntry(string? data)
    {
        TimeStamp = DateTime.Now;
        Data = data;
    }

    public DateTime TimeStamp;
    public readonly string? Data;
    
    public override string ToString()
    {
        return $"\"time\": \"{this.TimeStamp}\", \"data\": \"{this.Data}\"";
    }
}