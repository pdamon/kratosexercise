namespace Logger.Tests;

[TestFixture]
public class GetTests
{
    private List<LogEntry> _entries = null!;
    private StringWriter _writer = null!;
    private StringReader _reader = null!;
    private Get _get = null!;
    
    [SetUp]
    public void Setup()
    {
        _writer = new StringWriter();
        _reader = new StringReader("");
        _get = new Get();
        
        _entries = new List<LogEntry>();
    }

    [Test]
    public void Get_KeyBinds()
    {
        Assert.That(_get.CheckKeyBinds("G"), Is.True);
        Assert.That(_get.CheckKeyBinds("g"), Is.True);
        Assert.That(_get.CheckKeyBinds("q"), Is.False);
        Assert.That(_get.CheckKeyBinds("!"), Is.False);
        Assert.That(_get.CheckKeyBinds("Something Random"), Is.False);
    }

    [Test]
    public void Get_Print()
    {
        _get.Print(_writer);
        Assert.That(_writer.ToString(), Is.EqualTo("[G]et"));
    }

    [Test]
    public void Get_NoData()
    {
        _reader = new StringReader("\n");
        
        var rv1 = _get.Run(_entries, _reader, _writer);
        var output = _writer.ToString().Split("\n");
        
        Assert.That(rv1, Is.EqualTo(ReturnValue.Ok));
        
        Assert.That(output, Has.Length.EqualTo(5));
        Assert.That(output.ElementAt(0), Is.EqualTo("Enter the value you would like to search for:"));
        Assert.That(output.ElementAt(1), Is.EqualTo("Logged Data:"));
        Assert.That(output.ElementAt(2), Is.EqualTo("------------------------"));
        Assert.That(output.ElementAt(3), Is.EqualTo("------------------------"));
        Assert.That(output.ElementAt(4), Is.EqualTo(string.Empty));
    }
    
    [Test]
    public void Get_Simple()
    {
        _entries.Add(new LogEntry("1st data entry"));
        _reader = new StringReader("1st data entry");
        
        var rv1 = _get.Run(_entries, _reader, _writer);

        var output = _writer.ToString().Split("\n");
        
        Assert.That(rv1, Is.EqualTo(ReturnValue.Ok));

        Assert.That(output, Has.Length.EqualTo(6));
        Assert.That(output.ElementAt(0), Is.EqualTo("Enter the value you would like to search for:"));
        Assert.That(output.ElementAt(1), Is.EqualTo("Logged Data:"));
        Assert.That(output.ElementAt(2), Is.EqualTo("------------------------"));
        Assert.That(output.ElementAt(3)[..11], Is.EqualTo("1: \"time\": "));
        Assert.That(output.ElementAt(3)[^26..], Is.EqualTo(", \"data\": \"1st data entry\""));
        Assert.That(output.ElementAt(4), Is.EqualTo("------------------------"));
        Assert.That(output.ElementAt(5), Is.EqualTo(string.Empty));
    }

    [Test]
    public void Get_Complex()
    {
        _entries.Add(new LogEntry("1st data entry"));
        _entries.Add(new LogEntry("2nd thing with data"));
        _entries.Add(new LogEntry("3rd data entry"));
        
        _reader = new StringReader("data entry");
        
        var rv1 = _get.Run(_entries, _reader, _writer);
        var output = _writer.ToString().Split("\n");
        
        Assert.That(rv1, Is.EqualTo(ReturnValue.Ok));
        
        Assert.That(output, Has.Length.EqualTo(7));
        Assert.That(output.ElementAt(0), Is.EqualTo("Enter the value you would like to search for:"));
        Assert.That(output.ElementAt(1), Is.EqualTo("Logged Data:"));
        Assert.That(output.ElementAt(2), Is.EqualTo("------------------------"));
        Assert.That(output.ElementAt(3)[..11], Is.EqualTo("1: \"time\": "));
        Assert.That(output.ElementAt(3)[^26..], Is.EqualTo(", \"data\": \"1st data entry\""));
        Assert.That(output.ElementAt(4)[..11], Is.EqualTo("3: \"time\": "));
        Assert.That(output.ElementAt(4)[^26..], Is.EqualTo(", \"data\": \"3rd data entry\""));
        Assert.That(output.ElementAt(5), Is.EqualTo("------------------------"));
        Assert.That(output.ElementAt(6), Is.EqualTo(string.Empty));
    }
}